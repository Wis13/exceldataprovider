import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ExcelReader {
    private final String excelPathFile;
    private XSSFSheet sheet;
    private XSSFWorkbook book;
    private String sheetName;

    public ExcelReader(String excelPathFile) throws IOException {
        this.excelPathFile = excelPathFile;
        File file = new File(excelPathFile);
        try{
            FileInputStream fileInputStream = new FileInputStream(file);
            book = new XSSFWorkbook(fileInputStream);
            sheet = book.getSheet("Sheet1");
        } catch (IOException e){
            throw new IOException("Not supported format");
        }
    }

    public ExcelReader(String excelPathFile, String sheetName) throws IOException {
        this.excelPathFile = excelPathFile;
        this.sheetName = sheetName;
        File file = new File(excelPathFile);
        try{
            FileInputStream fileInputStream = new FileInputStream(file);
            book = new XSSFWorkbook(fileInputStream);
            sheet = book.getSheet(sheetName);
        } catch (IOException e){
            throw new IOException("Not supported format");
        }
    }

    public String cellToString(XSSFCell cell) throws Exception {
        Object result = null;
        CellType type =cell.getCellType();
        switch (type){
            case NUMERIC:
                result = cell.getNumericCellValue();
                break;
            case STRING:
                result = cell.getStringCellValue();
                break;
            case FORMULA:
                result = cell.getCellFormula();
            case BLANK:
                result = "";
            default:
                throw new Exception("Error reading cell");
        }
        return result.toString();
    }

    private int xlsxCountColumn(){
        return sheet.getRow(0).getLastCellNum();
    }

    private int xlsxCountRow(){
        return sheet.getLastRowNum() + 1;
    }

    public String[][] getSheetDataForTDD() throws Exception {
        File file = new File(excelPathFile);
        FileInputStream fileInputStream = new FileInputStream(file);
        book = new XSSFWorkbook(fileInputStream);
        sheet =book.getSheet("Sheet1");
        int numberOfColumn = xlsxCountColumn();
        int numberOfRow = xlsxCountRow();
        String[][] data = new String[numberOfRow - 1][numberOfColumn];
            for (int i = 1; i < numberOfRow; i++) {
                for (int j = 0; j < numberOfColumn; j++) {
                    XSSFRow row = sheet.getRow(i);
                    XSSFCell cell = row.getCell(j);
                    String value = cellToString(cell);
                    data[i - 1][j] = value;
                    if(value == null){
                        System.out.println("Empty cell");
                    }
                }
            }
        return data;
    }

    public String[][] getCustomSheetDataForTDD() throws Exception {
        File file = new File(excelPathFile);
        FileInputStream fileInputStream = new FileInputStream(file);
        book = new XSSFWorkbook(fileInputStream);
        sheet =book.getSheet(sheetName);
        int numberOfColumn = xlsxCountColumn();
        int numberOfRow = xlsxCountRow();
        String[][] data = new String[numberOfRow - 1][numberOfColumn];
        for (int i = 1; i < numberOfRow; i++) {
            for (int j = 0; j < numberOfColumn; j++) {
                XSSFRow row = sheet.getRow(i);
                XSSFCell cell = row.getCell(j);
                String value = cellToString(cell);
                data[i - 1][j] = value;
                if(value == null){
                    System.out.println("Empty cell");
                }
            }
        }
        return data;
    }


}
