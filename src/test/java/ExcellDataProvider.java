import org.testng.annotations.DataProvider;

import java.io.IOException;

public class ExcellDataProvider {

    @DataProvider
    public Object[][] userFromSheet1() throws Exception {
        String path = "src/main/resources/users.xlsx";
        ExcelReader excelReader = new ExcelReader(path);
        return excelReader.getSheetDataForTDD();
    }

    @DataProvider
    public Object[][] userFromlogin() throws Exception {
        String path = "src/main/resources/users.xlsx";
        ExcelReader excelReader = new ExcelReader(path, "login");
        return excelReader.getCustomSheetDataForTDD();
    }

    @DataProvider
    public Object[][] userForApi() throws Exception {
        String path = "src/main/resources/usersForReqres.xlsx";
        ExcelReader excelReader = new ExcelReader(path);
        return excelReader.getSheetDataForTDD();
    }


}
