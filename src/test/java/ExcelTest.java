import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class ExcelTest {

    @Test(dataProvider = "userFromSheet1", dataProviderClass = ExcellDataProvider.class)
    public void test(String param1, String param2){
    System.out.println("User with name " + param2 +" have id number " + param1);
    }

    @Test(dataProvider = "userFromlogin", dataProviderClass = ExcellDataProvider.class)
    public void test(String... param){
        System.out.println("Login " + param[0] +" password " + param[1]);
    }

    @Test(dataProvider = "userForApi", dataProviderClass = ExcellDataProvider.class)
    public void checkUsers(String... param){
        int id = (int) Double.parseDouble(param[0]);
        Response response = given()
                .contentType(ContentType.JSON)
                .get("https://reqres.in/api/users/" + id)
                .then().log().body().extract().response();

        JsonPath jsonPath = response.jsonPath();
        String email = jsonPath.getString("data.email");

        Assert.assertEquals(jsonPath.getInt("data.id"), id);
        Assert.assertEquals(jsonPath.getString("data.email"), param[1]);
        Assert.assertEquals(jsonPath.getString("data.first_name"), param[2]);
        Assert.assertEquals(jsonPath.getString("data.last_name"), param[3]);
        Assert.assertEquals(jsonPath.getString("data.avatar"), param[4]);
    }

}
